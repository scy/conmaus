# conmaus

The goal of this project is to build a "mouse driver" for the Linux text-only console (i.e. fbcon).
It should read mouse events from the hardware, display a cursor in the console, and inject xterm-compatible click/drag events.
In the end, it should feel like a GUI terminal, but without the GUI.

## Why not gpm?

[gpm](https://www.nico.schottelius.org/software/gpm/) displays a mouse cursor in the text console, supports different mice, and allows text selection, copy/paste, etc.

However, gpm does "its own thing":
It does not generate xterm-compatible events; instead, client applications need to be explicitly compatible to gpm's own protocol.

There is a pretty well-written GitHub issue [suggesting gpm to switch to xterm-style mouse reporting](https://github.com/telmich/gpm/issues/29), but it doesn't seem likely to happen any time soon.

## Status

I cannot stress this enough:
**Right now, this is just a proof of concept, not a finished piece of software.**

The code is undocumented, barely working and of alpha quality, but I think I will continue hacking on it in the future.

* Working:
  * Open a (currently hard-coded, ahem) `/dev/input/eventN` mouse, read relative position changes and translate them to a bounded cursor.
  * Retrieve the console size (columns and rows) in order to set the cursor boundaries.
  * Display that cursor on the console the same way gpm does: By using a one-cell "select text" ioctl.
  * Report press, release and drag events for the first three mouse buttons as well as mouse wheel up/down by injecting xterm "classic" or (configurable) 1006-mode (the default) control sequences into the console.
* Issues:
  * The event device (i.e. the "path to the mouse") should be configurable.
  * Maybe even auto-guess the device or read from multiple ones? Because I don't know how reboot-stable the device numbers are.
  * Hotplugging the device should be handled gracefully. Right now, conmaus will simply stop doing anything when you unplug the mouse, even if you re-plug it later.
  * After <kbd>Ctrl</kbd>+<kbd>C</kbd>, the `evdev` reading loop should terminate in a clean way.
  * Some kind of sensitivity setting and acceleration algorithms are badly needed.
  * Click reporting has no support for buttons other than first three ones (left, middle, right).
  * Resizing the terminal is not handled at all. (And yeah, the terminal size can change, e.g. by a change in rotation or font size.)
  * Switching to another virtual console won't disrupt the cursor (good), but click events apparently will continue to be reported to the console conmaus was started in.
  * conmaus is reporting mouse events without checking whether the running application actually can or wants to handle them. There is the `TIOCLINUX`/`TIOCL_GETMOUSEREPORTING` ioctl that may be able to help.
  * Apparently the recommended way of reporting mouse events to the console is not by injecting control sequences into the input stream, but by `TIOCLINUX`/`TIOCL_SETSEL`. However, I fear that this will cause its own set of issues, because as far as I can see, the kernel does not support mode 1006 and is therefore limited to 223 rows and columns each. There is a [promising set of patches by Tammo Block](https://lkml.org/lkml/2020/7/6/241) that aims to improve the situation, but has not quite made it into the kernel yet.

In other words:
This tool does _something_, and it looks promising enough as a proof of concept to warrant further development, but right now its usefulness is limited.

## Setting up

Because we're playing around with some restricted ioctls, conmaus most like has to be run as root.
(gpm requires that, too.)
Your distro might make it possible for you to get around that (e.g. with a `tty` group), but I haven't tested that yet.

The only external library that's required is [evdev](https://python-evdev.readthedocs.io/), install it using `pip` or whatever.

conmaus requires a somewhat recent version of Python 3.
I'm developing on 3.9.

For now(!), you'll have to edit `conmaus/main.py` to choose the evdev event you'd like to use.
I know, adding a command line option for it would've been easy, but I chose to write a nice readme instead and I think that's been a better use of my limited time.

If you don't know what device you'll have to use, the evdev library comes with a _very_ handy monitoring tool.
Run `python -m evdev.evtest`.

Start conmaus using something like `sudo python3 conmaus/main.py`.
It should print the size of your terminal and moving your mouse should move the cursor on screen.
Clicking one of the first three buttons or dragging (with a button pressed) currently prints the corresponding control sequence and sends that to the console.

By default, tmux will not accept mouse events when the terminal it's running in doesn't have mouse support listed in its terminfo, and if your `$TERM` is `linux` (most likely), then this will be the case.
However, adding `set -g terminal-overrides linux:kmous=\e[<` in your tmux config should solve this, allowing you to drag pane borders to resize, use the scroll wheel etc.
(Don't forget to also `set -g mouse on`, of course.)

## Contributions

Please refrain from actual code contributions right now, I'd like to work on the basics a bit more first.

However!
I would be _super_ happy if you could do some research or help me out with the more low-level, conceptual issues of actually passing the mouse events to clients.
Check out the "issues" section above, I'm especially interested in:

* What's the best way to send the events to the clients, that also works when switching virtual consoles?
* How should I detect console size changes? This is supposed to be a background daemon, I can't simply listen to `SIGWINCH`. (Maybe check how gpm does it?)
* Do you have suggestions on how to implement the sensitivity and acceleration stuff?

## Contact

Contact me [on Mastodon](https://mastodon.scy.name/@scy) or [on Twitter](https://twitter.com/scy).
Or open an issue in the [repo](https://codeberg.org/scy/conmaus).
