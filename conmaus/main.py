from console import Console
from input import EvMouse


def run(mouse_dev: str):
    con = Console()
    print(f"Terminal size is {con.width}x{con.height}.")

    def on_move(mouse):
        con.draw_cursor(mouse.x, mouse.y)
        if mouse.lowest_pressed is not None:  # Update while dragging.
            con.report_mouse_button(32, True, mouse.x, mouse.y)

    def on_button(mouse, btn, pressed):
        con.report_mouse_button(btn, pressed, mouse.x, mouse.y)

    def on_wheel_y(mouse, direction):
        con.report_mouse_button(4 if direction > 0 else 5, True, mouse.x, mouse.y)

    mouse = EvMouse(mouse_dev, con.width-1, con.height-1)
    mouse.on_move = on_move
    mouse.on_button = on_button
    mouse.on_wheel_y = on_wheel_y
    mouse.read_loop()


if __name__ == "__main__":
    run("/dev/input/event4")
