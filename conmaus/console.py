from fcntl import ioctl
from os import get_terminal_size
from struct import pack
from termios import TIOCLINUX, TIOCSTI


# TIOCLINUX subcode meaning "set selection", see ioctl_console(2).
TIOCL_SETSEL = 2

# sel_mode constant for TIOCL_SETSEL meaning "show mouse pointer".
# Undocumented in ioctl_console(2), taken from tiocl.h.
TIOCL_SELPOINTER = 3


class Console:

    MODE_CLASSIC = 1
    MODE_DECIMAL = 2

    def __init__(self, dev_path: str = "/dev/tty0", mode: int = MODE_DECIMAL):
        self.mode = int(mode)
        self.con = open(dev_path, "wb")
        self.width, self.height = get_terminal_size(self.con.fileno())

    def _inject_input(self, input: bytes):
        for pos in range(len(input)):
            ioctl(self.con, TIOCSTI, input[pos:pos+1])

    def draw_cursor(self, x: int, y: int):
        x += 1
        y += 1
        arg = pack("=Bhhhhh", TIOCL_SETSEL, x, y, x, y, TIOCL_SELPOINTER)
        ioctl(self.con, TIOCLINUX, arg)

    def report_mouse_button(self, button: int, pressed: bool, x: int, y: int):
        x += 1
        y += 1
        seq = b""
        if 4 <= button <= 5:
            button += 60
        if self.mode & self.MODE_CLASSIC:
            # Build "classic" (limited to 223 rows/cols) sequence.
            seq = b"\x1b[M" + bytes([32+(button if pressed else 3), 32+min(223,x), 32+min(223,y)])
        if self.mode & self.MODE_DECIMAL:
            # Append "decimal" (extension 1006) sequence.
            seq += f"\x1b[<{button};{x};{y}{'M' if pressed else 'm'}".encode()
        print(seq)
        self._inject_input(seq)


if __name__ == "__main__":
    from time import sleep
    con = Console()
    for y in range(1, con.height+1):
        for x in range(1, con.width+1):
            con.draw_cursor(x, y)
            sleep(0.001)
