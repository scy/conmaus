from evdev import InputDevice, ecodes


class EvMouse:

    BTN_NUMS = {
        ecodes.BTN_LEFT: 0,
        ecodes.BTN_MIDDLE: 1,
        ecodes.BTN_RIGHT: 2,
    }

    def __init__(self, evdev_path: str, max_x: int, max_y: int):
        self.dev = InputDevice(evdev_path)
        self.max_x = int(max_x)
        self.max_y = int(max_y)
        self.x = self.y = 0
        self.buttons = 0
        self.lowest_pressed = None

    def _cb(self, name, *args):
        cb = getattr(self, name, None)
        if callable(cb):
            try:
                cb(*args)
            except:
                pass

    def _set_button(self, button: int, pressed: bool):
        # Keep track of which buttons are held down.
        unchanged_others = self.buttons & ~(1 << button)
        button_bit = (1 if pressed else 0) << button
        self.buttons = unchanged_others | button_bit

        # Find the lowest one held down.
        lowest = None
        for i in range(3):
            if self.buttons & (1 << i) != 0:
                lowest = i
                break
        self.lowest_pressed = lowest

        # Callback.
        self._cb("on_button", self, button, pressed)

    def read_loop(self):
        for event in self.dev.read_loop():
            if event.type == ecodes.EV_REL:
                # Relative movement or mouse wheel.
                if event.code == ecodes.REL_X:
                    self.x = min(self.max_x, max(0, self.x + event.value))
                    self._cb("on_move", self)
                elif event.code == ecodes.REL_Y:
                    self.y = min(self.max_y, max(0, self.y + event.value))
                    self._cb("on_move", self)
                elif event.code == ecodes.REL_WHEEL:
                    self._cb("on_wheel_y", self, event.value)
            elif event.type == ecodes.EV_KEY:
                # Key (or mouse button!) press or release.
                btn_num = self.BTN_NUMS.get(event.code, None)
                if event.code is not None:
                    self._set_button(btn_num, event.value == 1)


if __name__ == "__main__":
    mouse = EvMouse("/dev/input/event4", 25, 25)
    mouse.on_move = lambda mouse: print(mouse.x, mouse.y)
    mouse.read_loop()
